chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  switch (request.message) {
    case "getNewNote":
      const note = getSelectedNode();
      const xpath = getXPath(note.node);

      // Создание DOM элементов
      const popup = createElement('div', 'extension-popup');
      const windowed = createElement('div', 'extension-window');
      const windowed_content = createElement('div', 'extension-content');
      const header = createElement('div', 'extension-header', '', '', 'Добавление заметки');
      const close = createElement('button', 'extension-popup-close', '', '');
      const name = createElement('label', '', '', '', 'Название заметки');
      const name_input = createElement('input', '', 'extension-content-title');
      const type = createElement('label', '', '', '', 'Тип заметки');
      const type_select = createElement('select', '', 'extension-content-permission');
      const type_option_one = createElement('option', '', '', '', 'Публичная');
      const type_option_two = createElement('option', '', '', '', 'Приватная');
      const permissions_div = createElement('div', 'extension-note-permissions');
      const comment = createElement('label', '', '', '', 'Комментарий к заметке (обязательно)');
      const comment_textarea = createElement('textarea', '', 'extension-content-note');
      const controller = createElement('div', 'extension-button');
      const controller_add = createElement('button', '', 'extension-content-send', '', 'Добавить');
      const controller_close = createElement('button', '', 'extension-content-close', '', 'Закрыть');

      // Доп. Стили
      windowed_content.style = "flex-flow: column; padding: 10px;";
      name_input.disabled = true;
      name_input.value = note.title;
      type_option_one.value = "0";
      type_option_two.value = "1";
      permissions_div.style = "display: none;";

      // Заполнение главной формы
      popup.appendChild(windowed);

      // Создание шапки
      windowed.appendChild(header);
      header.appendChild(close);

      // Создание контента окна
      windowed.appendChild(windowed_content);

      // Заполнение контента
      windowed_content.appendChild(name);
      windowed_content.appendChild(name_input);
      windowed_content.appendChild(type);
      windowed_content.appendChild(type_select);
      windowed_content.appendChild(permissions_div);
      windowed_content.appendChild(comment);
      windowed_content.appendChild(comment_textarea);
      windowed_content.appendChild(controller);
      type_select.appendChild(type_option_one);
      type_select.appendChild(type_option_two);
      controller.appendChild(controller_add);
      controller.appendChild(controller_close);

      for (var i = 0; i < 4; i++) {
        // Создание DOM элементов
        const permission__item = createElement('div', 'extension-note-permission__item');
        const permission__email = createElement('input');
        const permission__role = createElement('select', '', 'extension-content-permission');
        const permission__role_one = createElement('option', '', '', '', 'Администратор');
        const permission__role_two = createElement('option', '', '', '', 'Модератор');
        const permission__role_three = createElement('option', '', '', '', 'Пользователь');

        // Доп. Стили
        permission__item.style = "display:flex;";
        permission__email.placeholder = "email пользователя";
        permission__email.style = "flex: 2;";
        permission__role.style = "flex: 0;";
        permission__role_one.value = "1";
        permission__role_two.value = "7";
        permission__role_three.value = "10";

        // Формирование контента каждого из элементов
        permissions_div.appendChild(permission__item);
        permission__item.appendChild(permission__email);
        permission__item.appendChild(permission__role);
        permission__role.appendChild(permission__role_one);
        permission__role.appendChild(permission__role_two);
        permission__role.appendChild(permission__role_three);
      }

      $("body").append(popup);

      type_select.onchange = function(){
        if (type_select.selectedIndex == 1) {
          permissions_div.style.display = "block";
        } else {
          permissions_div.style.display = "none";
        }
      };

      controller_close.onclick = function() {
        popup.remove();
      };

      close.onclick = function() {
        popup.remove();
      };

      controller_add.onclick = function() {
        if(comment_textarea.value == ""){
          comment_textarea.style.borderColor = "#fe7064";
        } else {
          permissions = [];
          if (type_select.selectedIndex == 1) {
            Array.from(document.getElementsByClassName('extension-note-permission__item')).forEach(permission => {
              if (permission.childNodes[0].value != "") {
                const temp = []
                temp.push(permission.childNodes[0].value);
                temp.push(permission.childNodes[1].selectedOptions[0].value);
                permissions.push(temp);
              }
            });
          }
          console.log(permissions);
          chrome.runtime.sendMessage({
            message: "setNewNote",
            site: document.URL,
            title: name_input.value,
            XPath: xpath,
            type: type_select.selectedIndex,
            permissions: permissions,
            text: comment_textarea.value
          });
          popup.remove();
        }
      };
      break
    case "printNotes":
      if (document.evaluate) {
        var notes = request.notes;
        var grouped = groupBy(notes, item => item.path);
        grouped.forEach(group => {
          setWrapper(group);
        });
      }
      break
    case "drawNoteDetails":
      var notes = request.notes;
      console.log(notes);
      chrome.runtime.sendMessage({message: "getUserSession"}, function(response) {
        const popup = createElement('div', 'extension-popup');
        const windowed = createElement('div', 'extension-window');
        const windowed_content = createElement('div', 'extension-content');
        const header = createElement('div', 'extension-header', '', '', 'Просмотр заметки');
        const close = createElement('button', 'extension-popup-close', '', '');

        // К чему заметка 'Note Title'
        const title = createElement('div', 'extension-title');
        const title_text = createElement('span', 'extension-text', '', '', 'Заметка к:');
        const title_title = createElement('span', '', '', '', notes[0].title);

        const note_scroll = createElement('div', 'scrollbar-style');
        const comments_scroll = createElement('div', 'scrollbar-style');
        const note_comments = createElement('div', 'extension-note-comments');
        const note_comment_title = createElement('h1', '', '', '', 'Комментарии к заметке: ');
        const note_comments_editor = createElement('div', 'extension-note-editor');
        const note_comments_editor_textarea = createElement('textarea');
        const note_comments_editor_button = createElement('button', '', '', '', 'Отправить');

        popup.appendChild(windowed);

        // Создание шапки
        windowed.appendChild(header);
        header.appendChild(close);

        // Создание контента окна
        windowed.appendChild(windowed_content);

        // Создание заголовка заметки
        note_scroll.appendChild(title);
        title.appendChild(title_text);
        title.appendChild(title_title);

        // создание окна заметки
        windowed_content.appendChild(note_scroll);

        // Создание окна комментариев
        windowed_content.appendChild(note_comments);
        note_comments.appendChild(note_comment_title);
        note_comments.appendChild(comments_scroll);
        note_comments.appendChild(note_comments_editor);
        note_comments_editor.appendChild(note_comments_editor_textarea);
        note_comments_editor.appendChild(note_comments_editor_button);

        $("body").append(popup);

        notes.forEach(item => {
          const note = createElement('div', 'extension-note');
          const note_text = createElement('div', 'extension-note-text', '', item.text);
          const note_members = createElement('div', 'extension-note-members');
          const note_editor = createElement('div', 'extension-note-editor');
          const note_rating = createElement('div', 'extension-note-rating');


          note_scroll.appendChild(note);
          note.appendChild(note_text);
          note.appendChild(note_members);
          note.appendChild(note_editor);
          note_editor.appendChild(note_rating);
          console.log(item.rating_count);
          console.log(item.rating_count > 0);
          if (item.rating_count > 0) {
            const note_ratin_span = createElement('span', '', '', '', 'Рейтинг записи: ' + (item.rating_sum/item.rating_count).toFixed(2));
            const note_rating_background = createElement('div');
            const note_rating_bar = createElement('div');

            note_rating.appendChild(note_ratin_span);
            note_rating.appendChild(note_rating_background);
            note_rating_background.appendChild(note_rating_bar);

            note_rating_bar.style = "width: " + ((item.rating_sum * 100) / (item.rating_count * 5)) + "%;";
          } else {
            const note_ratin_span = createElement('span', '', '', '', 'Запись еще никто не оценивал...');
            note_rating.appendChild(note_ratin_span);
          }

          item.members.forEach(user => {
            const member = createElement('span', '', '', '', user.username);
            note_members.appendChild(member);
          });

          item.members.forEach(user => {
            if (user.username == response.data.username) {
              const button_del = createElement('button', '', '', '', 'Удалить');

              note_editor.appendChild(button_del);

              button_del.onclick = function() {
                chrome.runtime.sendMessage({
                  message: "deleteNote",
                  note_id: item.id
                });
              }
            }
          });

          // Получение и отображение комментариев
          chrome.runtime.sendMessage({ message: "getNoteComments", id: item.id }, function (response) {
            var comments = response.comments;
            comments.forEach(comment => {
              const comment_container = createElement('div', 'extension-note_comment');
              const comment_info = createElement('div', 'extension-note-comment__user');
              const comment_user = createElement('h4', '', '', '', comment.user.username);
              const comment_date = createElement('span', '', '', '', comment.updated_at);
              // comment_date.innerText(comment.create_data_time);

              const comment_text = createElement('span', 'extension-note-comment__text', '', comment.text);

              comments_scroll.appendChild(comment_container);
              comment_container.appendChild(comment_info);
              comment_container.appendChild(comment_text);
              comment_info.appendChild(comment_user);
              comment_info.appendChild(comment_date);
            });
            comments_scroll.scrollTop = comments_scroll.scrollHeight;
          });

          // Добавление комментариев
          note_comments_editor_button.onclick = function() {
            chrome.runtime.sendMessage({
              message: "addNewComment",
              note_id: item.id,
              text: note_comments_editor_textarea.value
            }, function(response) {
              console.log(response.status);
              if (response.status) {
                const comment_container = createElement('div', 'extension-note_comment');
                const comment_info = createElement('div', 'extension-note-comment__user');
                const comment_user = createElement('h4', '', '', '', response.data.user.username);
                const comment_date = createElement('span', '', '', '', response.data.updated_at);
                const comment_text = createElement('span', 'extension-note-comment__text', '', note_comments_editor_textarea.value);

                comments_scroll.appendChild(comment_container);
                comment_container.appendChild(comment_info);
                comment_container.appendChild(comment_text);
                comment_info.appendChild(comment_user);
                comment_info.appendChild(comment_date);

                comments_scroll.scrollTop = comments_scroll.scrollHeight;

                note_comments_editor_textarea.value = "";
              } else {
                alert("При отправке сообщения сервер выдал ошибку!");
              }
            });
          }
        });

        document.getElementsByClassName('extension-popup-close')[0].onclick = function() {
          document.getElementsByClassName('extension-popup')[0].remove();
        };
      });
      break
    default:
      break
  }
});

function groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
        var key = keyGetter(item);
        if (!map.has(key)) {
            map.set(key, [item]);
        } else {
            map.get(key).push(item);
        }
    });
    return map;
}

function getSelectedNode() {
  var selection = window.getSelection();
  if (selection.rangeCount > 0) {
    return {
      node: selection.getRangeAt(0).startContainer.parentNode,
      title: selection.toString()
    };
  }
}

function getXPath(element){
  if (element.nodeName == 'BODY') {
    return '/HTML/BODY';
  } else {
    var parentNode = Array.from(element.parentNode.childNodes).filter(node => node.nodeName == element.nodeName);
    var indexNode = parentNode.indexOf(element) + 1;

    return getXPath(element.parentNode) + '/' + element.nodeName + (parentNode.length > 1 ? '[' + indexNode + ']' : '' );
  }
}

function getElementByXpath(path) {
  return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

function setWrapper(items) {
  const original = getElementByXpath(items[0].path);
  const href = document.createElement('a');
  const temp_id = [];
  href.className = 'exnote';
  href.innerHTML = "[?]";
  items.forEach(item => {
    temp_id.push(item.id);
  });
  // href.dataset.id = temp_id;
  href.onclick = function () {
    chrome.runtime.sendMessage({
      message: "getNoteDetails",
      notes_id: temp_id
    });
  }

  var temp = original.innerText.split(items[0].title);
  original.innerHTML = "";
  original.append(temp[0] + items[0].title);
  original.append(href);
  original.append(temp[1]);
}

function createElement(tagName = 'div', className = '', id = '',  innerHTML = '', innerText = '') {
    let element = document.createElement(tagName);
    if (id)        element.id = id;
    if (className) element.className = className;
    if (innerHTML) element.innerHTML = innerHTML;
    if (innerText) element.innerText = innerText;
    return element;
}
