// Settings
var server = '94.103.81.185';

function renderLoading(parent) {
  hidden('loading');
  unhidden(parent);
}
function renderError(parent, go, error) {
  hidden('error');
  unhidden(parent);
  document.getElementById('error_message').innerHTML = error;
  setTimeout(function() {
    unhidden('hidden');
    renderSignIn(go);
  }, 2500);
}
function renderSignUp(parent) {
  unhidden(parent);
  hidden('sign-up');
  const button_sreg = document.getElementById('registration_button');
  const button_login = document.getElementById('login_page');
  if (button_sreg && button_login) {
    button_sreg.addEventListener('click', sendRegistartion);
    button_login.addEventListener('click', function(){ renderSignIn('sign-up'); console.log('Sign In Activated!'); });
  }
}
function renderSignIn(parent) {
  unhidden(parent);
  hidden('sign-in');
  const button_send = document.getElementById('send_button');
  const button_reg = document.getElementById('registration_page');
  if (button_send && button_reg) {
    button_send.addEventListener('click', sendAuth);
    button_reg.addEventListener('click', function(){ renderSignUp('sign-in') });
  }
}

function renderNotes(parent) {
  unhidden(parent);
  document.getElementById('notes-exit').addEventListener("click", function() {
    renderProfile('notes');
  });

  var profile = JSON.parse(atob(JSON.parse(localStorage.getItem('auth')).token.split('.')[1]))

  $.ajax({
    type: "GET",
    url: 'http://' + server + '/api/v1.0/user-notes/',
    headers: {
        'Authorization': 'JWT ' + JSON.parse(localStorage.getItem('auth')).token,
    },
    contentType: 'application/json',
    dataType: 'json',
    success: (notes) => {
      document.getElementById('note-list').innerHTML = '';
      notes.forEach(item => {
        var title = document.createElement('span');
        title.appendChild(document.createTextNode(item['title'].slice(0,33)));

        var site = document.createElement('span');
        site.appendChild(document.createTextNode(item['site'].slice(0,50)));

        var note = document.createElement('button');
        note.appendChild(title);
        note.appendChild(site);
        note.xpath = item['marker'];
        note.addEventListener('click', function() {
          chrome.tabs.create({ url: item['site'] }, function(tab){ });
        });
        document.getElementById('note-list').appendChild(note);
      });
    },
    error: function(xhr,textStatus,err) {}
  });
  hidden('notes');
}
function renderProfile(parent) {
  unhidden(parent);
  hidden('profile');

  var profile = JSON.parse(atob(JSON.parse(localStorage.getItem('auth')).token.split('.')[1]))

  if (document.getElementById('profile') != null) {
    document.getElementById('profile-name').innerHTML = profile.username;
    document.getElementById('profile-email').innerHTML = profile.email;
  }
}
function unhidden(element){
  var valid = document.getElementById(element);
  if (valid != null) {
    if(!valid.classList.contains('hidden')) {
      document.getElementById(element).classList.add('hidden');
    }
  }
}
function hidden(element){
  var valid = document.getElementById(element);
  if (valid != null){
    if(valid.classList.contains('hidden')) {
      document.getElementById(element).classList.remove('hidden');
    }
  }
}

function sendAuth(event) {
  renderLoading('sign-in');
  $.ajax({
    type: "POST",
    url: 'http://' + server + '/api/v1.0/auth/login/',
    data: JSON.stringify({
      'username': document.getElementById('login').value,
      'password': document.getElementById('password').value
    }),
    contentType: 'application/json',
    dataType: 'json',
    success: (data) => {
      localStorage.setItem('auth', JSON.stringify({'token': data.token, 'verify': true}));
      renderProfile('loading');
    },
    error: function(xhr,textStatus,err)
    {
      if (xhr.status == 0) {
        renderError('loading', 'sign-in', 'Server is Offline');
      } else {
        renderError('loading', 'sign-in', xhr.responseText);
      }
    }
  });
}
function sendRegistartion(event) {
  renderLoading('sign-up');
  $.ajax({
    type: "POST",
    url: 'http://' + server + '/api/v1.0/auth/registration/',
    data: JSON.stringify({
      'username': document.getElementById('reg_login').value,
      'email': document.getElementById('reg_email').value,
      'password': document.getElementById('reg_password').value
    }),
    contentType: 'application/json',
    dataType: 'json',
    success: (data) => {
      renderSignIn('loading');
    },
    error: function(xhr,textStatus,err)
    {
      if (xhr.status == 0) {
        renderError('loading', 'sign-up', 'Server is Offline');
      } else {
        renderError('loading', 'sign-up', xhr.responseText);
      }
    }
  });
}
function verify_token(event) {
  renderLoading('sign-in');
  if (localStorage.getItem('auth') != null) {
    if (JSON.parse(localStorage.getItem('auth')).verify == false ) {
      $.ajax({
        type: "POST",
        url: 'http://' + server + '/api/v1.0/auth/verify/',
        data: JSON.stringify({
          'token': JSON.parse(localStorage.getItem('auth')).token
        }),
        contentType: 'application/json',
        dataType: 'json',
        success: (date) => {
          localStorage.setItem('auth', JSON.stringify({'token': JSON.parse(localStorage.getItem('auth')).token, 'verify': true}));
          renderProfile('loading')
        },
        error: function(xhr,textStatus,err) {
          if (xhr.status == 0) {
            renderError('loading', 'sign-in', 'Server is Offline');
          } else {
            renderError('loading', 'sign-in', xhr.responseText);
          }
        }
      });
    } else if (JSON.parse(localStorage.getItem('auth')).token != '') {
      renderProfile('loading')
    }
  } else {
    renderSignIn('loading');
  }
}

document.addEventListener('DOMContentLoaded', function() {
  verify_token();

  if (document.getElementById('profile') != null) {
    // События на кнопки в меню расширения
    document.getElementById('profile-notes').addEventListener("click", function() {
      renderNotes('profile');
    });
    document.getElementById('profile-settings').addEventListener("click", function() {
      chrome.runtime.openOptionsPage();
    });
    document.getElementById('profile-exit').addEventListener("click", function() {
      localStorage.removeItem('auth');
      renderSignIn('profile');
    });
  }
});
chrome.contextMenus.onClicked.addListener(function(info, tab) {
    if (info.menuItemId == 'addnote') {
      chrome.tabs.sendMessage(tab.id, {message: "getNewNote"});
    }
});
function loadNote(tab){
  $.ajax({
    type: "GET",
    url: 'http://' + server + '/api/v1.0/notes/',
    data: {
      'site': tab.url
    },
    headers: {
        'Authorization': 'JWT ' + JSON.parse(localStorage.getItem('auth')).token,
    },
    contentType: 'application/json',
    dataType: 'json',
    success: (date) => {
      if(date != null) {
        chrome.tabs.sendMessage(tab.id, {message: "printNotes", notes: date });
      }
    },
    error: () => {}
  });
}

// Chrome API
chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
  if (changeInfo.status == 'complete') {
    if (localStorage.getItem('auth') != null) {
      if (JSON.parse(localStorage.getItem('auth')).verify == true) {

        chrome.tabs.getSelected(null, function(tab) {
            loadNote(tab);
        });
        chrome.contextMenus.removeAll();
        chrome.contextMenus.create({
              title: 'Добавить заметку',
              id: 'addnote',
              contexts: ['selection'],
        });
      }
    }
  }
});

chrome.windows.onRemoved.addListener(function(windowId){
  localStorage.setItem('auth', JSON.stringify({'token': JSON.parse(localStorage.getItem('auth')).token, 'verify': false}));
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
switch (request.message) {
    case "setNewNote":
    console.log(JSON.stringify({
      'site': request.site,
      'text': request.text,
      'title': request.title,
      'path': request.XPath,
      'members': request.permissions
    }));
      $.ajax({
        type: "POST",
        url: 'http://' + server + '/api/v1.0/notes/',
        data: JSON.stringify({
          'site': request.site,
          'text': request.text,
          'title': request.title,
          'path': request.XPath,
          'members': request.permissions
        }),
        headers: {
            'Authorization': 'JWT ' + JSON.parse(localStorage.getItem('auth')).token
        },
        contentType: 'application/json',
        dataType: 'json',
        success: function(date) {

        },
        error: function(xhr,textStatus,err) {
          alert(xhr.responseText);
        }
      });
      break
    case "getNoteDetails":
      $.ajax({
        type: "GET",
        url: 'http://' + server + '/api/v1.0/note/detail/',
        data: {
          'notes': JSON.stringify({'id': request.notes_id})
        },
        headers: {
            'Authorization': 'JWT ' + JSON.parse(localStorage.getItem('auth')).token,
        },
        contentType: 'application/json',
        dataType: 'json',
        success: (data) => {
          chrome.tabs.getSelected(null, function(tab) {
            chrome.tabs.sendMessage(tab.id, {message: "drawNoteDetails", notes: data });
          });
        },
        error: () => {}
      });
      break
    case "getNoteComments":
      $.ajax({
        type: "GET",
        async: false,
        url: 'http://' + server + '/api/v1.0/note/detail/comments',
        data: {
          'note': request.id
        },
        headers: {
            'Authorization': 'JWT ' + JSON.parse(localStorage.getItem('auth')).token,
        },
        contentType: 'application/json',
        dataType: 'json',
        success: (data) => {
          sendResponse({comments: data});
        },
        error: () => {}
      });
      break
    case "getUserSession":
      var profile = JSON.parse(atob(JSON.parse(localStorage.getItem('auth')).token.split('.')[1]));
      sendResponse({data: profile});
      break
    case "deleteNote":
      $.ajax({
        type: "POST",
        url: 'http://' + server + '/api/v1.0/note/delete/',
        data: JSON.stringify({ 'id': request.note_id }),
        headers: {
            'Authorization': 'JWT ' + JSON.parse(localStorage.getItem('auth')).token,
        },
        contentType: 'application/json',
        dataType: 'json',
        success: () => {
          chrome.tabs.getSelected(null, function(tab) {
            var code = 'window.location.reload();';
            chrome.tabs.executeScript(tab.id, {code: code});
          });
        },
        error: () => {
          chrome.tabs.getSelected(null, function(tab) {
            var code = 'window.location.reload();';
            chrome.tabs.executeScript(tab.id, {code: code});
          });
        }
      });
      break
    case "addNewComment":
      console.log(JSON.stringify({ 'note_id': request.note_id, 'text': request.text }));
      $.ajax({
        type: "POST",
        url: 'http://' + server + '/api/v1.0/note/detail/comments/add/',
        data: JSON.stringify({ 'note_id': request.note_id, 'text': request.text }),
        async: false,
        headers: {
            'Authorization': 'JWT ' + JSON.parse(localStorage.getItem('auth')).token,
        },
        contentType: 'application/json',
        dataType: 'json',
        success: (data) => {
          sendResponse({ status: true, data: data});
        },
        error: () => {
          sendResponse({ status: false });
        }
      });
      break
    default:
      break
  }
});
