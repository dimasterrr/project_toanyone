from rest_framework import status
from rest_framework.decorators import permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

from django.contrib.auth.models import User
from note.models import Note, Permission, Role, Comment, NoteRating
from note.serializer import NoteSerializer, UserSerializer, CommentSerializer

import json
import codecs


class SiteNotes(APIView):
    def get(self, request, format=None):
        items = Note.objects.filter(site=request.GET.get('site', None))
        item_list = []
        for item in items:
            if item.type == 0:
                item_list.append(item)
            else:
                if request.user in item.members.all():
                    item_list.append(item)
        serializer = NoteSerializer(item_list, many=True)
        if items.exists():
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

    def post(self, request, format=None):
        data = json.loads(request.body.decode('utf-8'))
        serializer = NoteSerializer(data=data)
        if serializer.is_valid():
            note = serializer.save()
            Permission.objects.create(user=request.user, note=note, role_id=1)
            for member in data['members']:
                user = User.objects.filter(email=member[0]).first()
                if user:
                    Permission.objects.create(user=user, note=note, role_id=member[1])
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetailsNote(APIView):
    def get(self, request, format=None):
        data = json.loads(request.GET.get('notes', None))
        items = Note.objects.filter(pk__in=data["id"])
        serializer = NoteSerializer(items, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class NoteComments(APIView):
    def get(self, request, format=None):
        note_id = request.GET.get('note', None)
        note = Note.objects.filter(pk=note_id).first()
        comments = Comment.objects.filter(note=note)
        if comments:
            serializer = CommentSerializer(comments, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_204_NO_CONTENT)


class AddComment(APIView):
    def post(self, request, format=None):
        data = json.loads(request.body.decode('utf-8'))
        comment = Comment(note_id=data["note_id"], user=request.user, text=data["text"])
        comment.save()
        if comment:
            serializer = CommentSerializer(comment, many=False)
            return Response(serializer.data, status=status.HTTP_200_OK) 
        return Response(status=status.HTTP_400_BAD_REQUEST)


class DeleteNote(APIView):
    def post(self, request, format=None):
        data = json.loads(request.body.decode('utf-8'))
        item = Note.objects.filter(pk=data['id']).first()
        if item:
            if request.user in item.members.all():
            	item.delete()
            	return Response(status=status.HTTP_200_OK) 
            return Response(status=status.HTTP_403_FORBIDDEN)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class UserNotes(APIView):
    def get(self, request, format=None):
        user_notes = Note.objects.filter(members__in=[request.user.id])
        serializer = NoteSerializer(user_notes, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ChangeRating(APIView):
    def post(self, request, format=None):
        data = json.loads(request.body.decode('utf-8'))
        note = Note.objects.filter(pk=data['id']).first()
        if note and data['rating'] >= 0 and data['rating'] <=5:
            rating = NoteRating.objects.filter(note=note, user=request.user).first()
            if rating:
                rating.rating = data['rating']
                rating.save()
                return Response(status=status.HTTP_200_OK)
            NoteRating.objects.create(note=note, user=request.user, rating=data['rating'])
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class Registration(APIView):
    permission_classes = (AllowAny, )
    authentication_classes = ()
    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)