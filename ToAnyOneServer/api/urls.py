from django.conf.urls import include
from django.urls import path
from rest_framework import routers
from .views import SiteNotes, UserNotes, DetailsNote, DeleteNote, Registration, NoteComments, AddComment, ChangeRating
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from django.views.decorators.csrf import csrf_exempt


router = routers.DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
    path('notes/', SiteNotes.as_view(), name='note-list'),
    path('note/detail/', DetailsNote.as_view(), name='note-details'),
    path('note/detail/rating/', ChangeRating.as_view(), name='note-rating'),
    path('note/detail/comments/', NoteComments.as_view(), name='note-comments'),
    path('note/detail/comments/add/', AddComment.as_view(), name='note-comment-add'),
    path('note/delete/', DeleteNote.as_view(), name='note-details'),
    path('user-notes/', UserNotes.as_view(), name='user-notes'),
    path('auth/login/', obtain_jwt_token, name='user-notes'),
    path('auth/refresh/', refresh_jwt_token, name='login'),
    path('auth/verify/', verify_jwt_token, name='verify'),
    path('auth/registration/', csrf_exempt(Registration.as_view()), name='registration')
]
