from .models import Note, Comment, NoteRating
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework.fields import CurrentUserDefault
from django.contrib.auth.models import User
from django.db.models import Sum


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    username = serializers.CharField(
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class NoteSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    created_at = serializers.ReadOnlyField()
    updated_at = serializers.ReadOnlyField()
    members = UserSerializer(many=True, read_only=True)
    rating_sum = serializers.SerializerMethodField()
    rating_count = serializers.SerializerMethodField()

    def create(self, validated_data):
        note = Note.objects.create(
            site  = validated_data['site'],
            text  = validated_data['text'],
            title = validated_data['title'],
            path  = validated_data['path'],
        )
        note.save()
        return note

    def get_rating_sum(self, obj):
        rating_obj = NoteRating.objects.filter(note=obj)
        if rating_obj:
            return rating_obj.aggregate(sum=Sum('rating'))['sum']
        return 0

    def get_rating_count(self, obj):
        return NoteRating.objects.filter(note=obj).count()

    class Meta:
        model = Note
        fields = ('id', 'site', 'text', 'title', 'path', 'created_at', 'updated_at', 'members', 'rating_sum', 'rating_count')


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()
    user = UserSerializer()

    class Meta:
        model = Comment
        fields = ('id', 'user', 'text', 'created_at', 'updated_at')