from django.contrib import admin
from .models import Note, Permission, Role, Comment, NoteRating

# Register your models here.


class PermissionM2MInline(admin.TabularInline):
    model = Permission
    extra = 1


class PermissionAdmin(admin.ModelAdmin):
    inlines = (PermissionM2MInline,)


admin.site.register(Note, PermissionAdmin)
admin.site.register(NoteRating)
admin.site.register(Permission)
admin.site.register(Role)
admin.site.register(Comment)
