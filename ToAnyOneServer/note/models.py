from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User

from datetime import datetime


note_type = (
    (0, 'Публичный'),
    (1, 'Приватный'),
)


class Role(models.Model):
    name = models.CharField(max_length=25)

    def __str__(self):
        return self.name


class Note(models.Model):
    members = models.ManyToManyField(User, through='Permission', through_fields=('note', 'user'))
    type = models.IntegerField(choices=note_type, default=0)
    site = models.TextField(default=None)
    title = models.TextField(default=None)
    text = models.TextField(default=None)
    path = models.TextField(default=None)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.site


class Permission(models.Model):
    note = models.ForeignKey(Note, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)


class Comment(models.Model):
    note = models.ForeignKey(Note, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField(default=None)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)


class NoteRating(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    note = models.ForeignKey(Note, on_delete=models.CASCADE)
    rating = models.IntegerField(default=0, validators=[MaxValueValidator(5), MinValueValidator(0)])